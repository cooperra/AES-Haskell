module Main where

--This file contains functions to read input and main to compile the program
    --The functions read command line options, open a file, and then encrypt or
    --decrypt the contents of the file with AES

import AES
import System.Environment
import System.IO
import Options.Applicative
import Data.Semigroup ((<>))
import Numeric

--Conversion and reading functions to get input and provide output
readhexstr :: [Char] -> [Int]
readhexstr [] = []
readhexstr [a] = [] --gets rid of errant newlines
readhexstr (a:b:cs) = (read ('0':'x':a:[b])::Int):(readhexstr cs) -- add a 0x to the beginning then read the lot as an int

--This function converts all integers to hexstrings
tohexstr :: [Int] -> [[Char]]
tohexstr = map (\x -> showHex x "") -- apply showHex to every integer in the list

--This function adds a leading 0 to values less than 0x10 for ouput
addzeroes :: [[Char]] -> [[Char]]
addzeroes = map (\x -> if length x == 1 then '0':x else x)

--This function converts a list of ints to a hexstring where all values are concatenated together
showhexstr :: [Int] -> [Char]
showhexstr = concat . addzeroes . tohexstr

--This is a data structure to hold the values supplied as command line options
data AESoptions = AESoptions
  { mode :: Bool, --cbc if true, ecb if false
    action :: Bool, -- decryption if true, encryption if false
    key :: String, -- stores the key for whichever action
    filename :: String } -- filename of where to get data to encrypt or decrypt

--This function reads each command line option and returns the AESoptions data structure with the correct value set
optparse :: Parser AESoptions
optparse = AESoptions
    <$> switch
        ( long "cbc"
       <> short 'c'
       <> help "Use Cipher Block Chaining rather than Electronic Code Book" )
    <*> switch
        ( long "decrypt"
       <> short 'd'
       <> help "Tells the program to decrypt instead of encrypt" )
    <*> strOption
        ( long "key"
       <> short 'k'
       <> help "The key for AES encryption or decryption" )
    <*> strOption
        ( long "file"
       <> short 'f'
       <> help "The file to encrypt or decrypt" )

--This function reads the command line options and then gives those inputs to the dothething function
main :: IO ()
main = dothething =<< execParser opts
    where
        opts = info (optparse <**> helper) -- Call option parser and provide some extra data for incorrect options or help option
          ( fullDesc
         <> progDesc "Run AES to encrypt or decrypt a file"
         <> header "cooperraAES - MATH 4175 Crypto I final project" )

--This function reads the file with the contents to encrypt/decrypt and then calls the right function for specified operation and mode
    --Then it outputs the result to stdout
dothething :: AESoptions -> IO ()
dothething (AESoptions cbc dec key filename) = do
    handle <- openFile filename ReadMode --open file
    contents <- hGetContents handle -- read file
    let temp = readhexstr contents -- turn file contents into ints
    --below option does padding if necessary
    let blocks = temp ++ replicate (if (16 - ((length temp) `mod` 16)) == 16 then 0 else (16 - ((length temp) `mod` 16))) (16 - ((length temp) `mod` 16))
    if dec then --if decryption option
        if cbc then --if cbc decryption
            putStrLn (showhexstr $ dcbc (readhexstr key) blocks [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
        else -- if ecb encryption
            putStrLn (showhexstr $ decb (readhexstr key) blocks)
    else
        if cbc then --if cbc encryption
            putStrLn (showhexstr $ ecbc (readhexstr key) blocks [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
        else -- if ecb encryption
            putStrLn (showhexstr $ eecb (readhexstr key) blocks)
    hClose handle
