To operate this program:

cooperraAES [-c|--cbc] [-d|--decrypt] -k|--key <key in ascii hex> -f|--file <file with plaintext or ciphertext>

This program encrypts/decrypts in electronic code book mode by default.
Use the -c or --cbc flags to tell it to use cipher block chaining mode.

The program does encryption by default.  Use the -d or --decrypt flags to tell it to decrypt.
The argument after the -k or --key flags specifies the key.  This value is not a file name.
The argument after the -f or --file flags specifies the file name of the file to encrypt or decrypt

To compile:

uses the Glasgow Haskell Compiler (GHC) obtained from haskell-platform package

ghc -o cooperraAES Main.hs
