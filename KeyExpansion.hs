module KeyExpansion (expansion128, expansion192, expansion256, permute) where

--This file contains the functions to perform key expansion for the different key
  --lengths.  It exports the expansion functions, which operate on lists of integers.
  --Each integer in the list represents one byte.

import Data.Bits   --For bitwise operations
import Sbox
import BinaryField --For remainder
import Numeric     --For hex string conversions

--Moves the elements in list as to positions indicated by list xs
--Performs no length checking
permute  :: [a] -> [Int] -> [a]
permute as xs = map (as!!) xs

--The Key Expansion Core
core :: [Int] -> Int -> [Int]
core xs round = zipWith xor [remainder (shiftL 1 round) 0x11b,0,0,0] $ map sbox $ permute xs [1,2,3,0]

--Gets four bytes for key expansion has 3 cases for when to pass temp1 through the core, the sbox, or otherwise
get4 :: Int -> Int -> Int -> [Int] -> [Int]
get4 len itr round xs
  | itr == 0 = (zipWith xor (core (drop (length xs - 4) xs) round) (take 4 $ drop (length xs -len) xs))
  | len == 32 && itr == 4 = (zipWith xor (map sbox (drop (length xs - 4) xs)) (take 4 $ drop (length xs -len) xs))
  | otherwise = (zipWith xor (drop (length xs - 4) xs) (take 4 $ drop (length xs -len) xs))

--Manages the inputs for get4 and calls get4 until we have all of the round keys
exaux :: Int -> Int -> Int -> [Int] -> Int -> Int -> [Int]
exaux len itr round xs totallen itrs
  | length xs >= totallen = xs --Checks if we have all of the bytes we need
  | itr == itrs = exaux len 1 (round + 1) (xs ++ (get4 len 0 (round + 1) xs)) totallen itrs
  | otherwise = exaux len (itr+1) round (xs ++ (get4 len itr round xs)) totallen itrs

--These functions take the key as a list of integers to expand it and gives inputs to exaux
expansion128 :: [Int] -> [Int]
expansion128 xs = exaux 16 0 0 xs 176 4

expansion192 :: [Int] -> [Int]
expansion192 xs = exaux 24 0 0 xs 208 6

expansion256 :: [Int] -> [Int]
expansion256 xs = exaux 32 0 0 xs 240 8
