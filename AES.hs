module AES (eecb, ecbc, decb, dcbc) where

--This file contains all of the AES functions. Note, every fourth element in the
    --block starts a new column in the AES state
    --The functions to do ecb and cbc encryption and decryption are exported to other modules

import KeyExpansion
import Sbox
import BinaryField
import Data.Bits
import Numeric

--Encryption/Decryption operations on the block

--This function xors each element of the round key with the corresponding element in the block
addRoundKey :: [Int] -> [Int] -> [Int]
addRoundKey key block = zipWith xor key block

--Encryption operations

--This function shifts each row in the AES state to the left according to row number
    --Row 0 is not shifted, row 1 shifted by 1, row 2 shifted by 2, row 3 shifted by 3
shiftRows :: [Int] -> [Int]
shiftRows block = permute block [0,5,10,15,4,9,14,3,8,13,2,7,12,1,6,11]

--This function passes each element of the block through the sbox
subBytes :: [Int] -> [Int]
subBytes = map sbox

--This function mixes one column at a time with matrix multiplication over a binary field
    --Turns the provided column into four lists of ints and multiplies each element of each row in
    --the mix columns matrix with the elements of the four copies of the column
    --then it xors them all together
mixcol :: [Int] -> [Int]
mixcol col = map (\res -> foldr xor 0 res) $ zipWith (zipWith (\x y -> multiplyint x y 0x11b)) [[2,3,1,1],[1,2,3,1],[1,1,2,3],[3,1,1,2]] (take 4 $ repeat col)

--This function recurses over each column to mix all four columns and return the new state
mixColumns :: [Int] -> [Int]
mixColumns [] = []
mixColumns (w:x:y:z:col) = (mixcol [w,x,y,z]) ++ (mixColumns col)

--This function combines all of the encryption functions for a full round
    --Does subBytes first, then shiftRows, then mixColumns, then addRoundKey and feeds that into the next loop iteration
loop :: [Int] -> [Int] -> Int -> [Int]
loop keys block itr
  | itr == 0 = block
  | otherwise = loop (drop 16 keys) (addRoundKey (take 16 keys) $ mixColumns $ shiftRows $ subBytes block)  (itr-1)

--This function combines loop with the initial round and final round to encrypt one block
encryptioncore :: [Int] -> [Int] -> Int -> Int -> [Int]
encryptioncore keys block klen rounds = addRoundKey (drop (length keys - 16) keys) $ shiftRows $ subBytes $ loop (drop (16) keys) (addRoundKey (take 16 keys) block) (rounds - 1)

--This function measures the length of the key to determine what inputs to give to encryptioncore
encrypt :: [Int] -> [Int] -> [Int]
encrypt key block
 | 16 == length key = encryptioncore (expansion128 key) block 16 10
 | 24 == length key = encryptioncore (expansion192 key) block 24 12
 | 32 == length key = encryptioncore (expansion256 key) block 32 14
 | otherwise = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

--This function recurses over all blocks to encrypt them.  Electronic Code Book mode
eecb :: [Int] -> [Int] -> [Int]
eecb key [] = []
eecb key blocks = (encrypt key (take 16 blocks)) ++ eecb key (drop 16 blocks)

--This function recurses over all blocks to encrypt them.  Cipher Block Chaining mode
ecbc :: [Int] -> [Int] -> [Int] -> [Int]
ecbc _ [] _ = []
ecbc key blocks prev = (encrypt key $ zipWith xor prev $ take 16 blocks) ++ ecbc key (drop 16 blocks) (encrypt key $ zipWith xor prev $ take 16 blocks)


--Decryption operations

--This function reverses all of the keys provided by key expansion for decryption
revkeys :: [Int] -> [Int]
revkeys [] = []
revkeys (a:b:c:d:e:f:g:h:i:j:k:l:m:n:o:p:xs) = revkeys xs ++ [a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p]

--This function performs the inverse sbox function on all elements of the block
invSubBytes :: [Int] -> [Int]
invSubBytes = map invSbox

--This function shifts each row to the right by row number
invShiftRows :: [Int] -> [Int]
invShiftRows block = permute block [0,13,10,7,4,1,14,11,8,5,2,15,12,9,6,3]

--This function multiplies a column with the inverse matrix from mixcol
    --Does the exact same thing as mixColumns with a different matrix
invmixcol :: [Int] -> [Int]
invmixcol col = map (\res -> foldr xor 0 res) $ zipWith (zipWith (\x y -> multiplyint x y 0x11b)) [[0xe,0xb,0xd,0x9],[0x9,0xe,0xb,0xd],[0xd,0x9,0xe,0xb],[0xb,0xd,0x9,0xe]] (take 4 $ repeat col)

--This function recurses over each column to unmix them all
invMixColumns :: [Int] -> [Int]
invMixColumns [] = []
invMixColumns (w:x:y:z:col) = (invmixcol [w,x,y,z]) ++ (invMixColumns col)

--This function combines all of the decryption functions for a single round
decloop :: [Int] -> [Int] -> Int -> [Int]
decloop keys block itr
  | itr == 0 = block
  | otherwise = decloop (drop 16 keys) (invSubBytes $ invShiftRows $ invMixColumns $ addRoundKey (take 16 keys) block)  (itr-1)

--This function combines the initial round, the loop, and the final round to decrypt a block
decryptioncore :: [Int] -> [Int] -> Int -> Int -> [Int]
decryptioncore keys block klen rounds = addRoundKey (drop (length keys - 16) keys) $ decloop (drop (16) keys) (invSubBytes $ invShiftRows $ (addRoundKey (take 16 keys) block)) (rounds - 1)

--This function provides the correct input to the decryption core based on key length
decrypt :: [Int] -> [Int] -> [Int]
decrypt key block
 | 16 == length key = decryptioncore (revkeys $ expansion128 key) block 16 10
 | 24 == length key = decryptioncore (revkeys $ expansion192 key) block 24 12
 | 32 == length key = decryptioncore (revkeys $ expansion256 key) block 32 14
 | otherwise = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

--This function recurses over all blocks to decrypt them.  Electronic Code Book mode
decb :: [Int] -> [Int] -> [Int]
decb key [] = []
decb key block = (decrypt key (take 16 block)) ++ decb key (drop 16 block)

--This function recurses over all blocks to decrypt them.  Cipher Block Chaining mode
dcbc :: [Int] -> [Int] -> [Int] -> [Int]
dcbc _ [] _ = []
dcbc key blocks prev = (zipWith xor prev $ decrypt key $ take 16 blocks) ++ dcbc key (drop 16 blocks) (take 16 blocks)
