module BinaryField (multiply, multiplyint, multiplyaux, division, remainder, inversion, inverse) where
import Data.Bits
import Numeric

--This file contains the code for the homework about binary field multiplication
    --and inverse.  The multiplication and remainder functions are reused in
    --the key expansion and mixColumns functions for AES

--calculates the length in bits of an integer
bitlength :: Int -> Int
bitlength 0 = 0
bitlength x = 1 + bitlength (shiftR x 1)

--shifts integer y to have the same length as x
extend :: Int -> Int -> Int
extend x y = shiftL y ((bitlength x) - (bitlength y))

--adds '0x' to a string of hex characters and then reads the value as an int
fromstring :: String -> Int
fromstring s = read ("0x" ++ s) :: Int

--Functions for binary field multiplication

--This function does normal multiplication between two numbers
multiplyaux :: Int -> Int -> Int
multiplyaux x 0 = 0
multiplyaux x y
    | y == 0 = 0
    | even y = multiplyaux (shiftL x 1) (shiftR y 1)
    | otherwise = xor x (multiplyaux (shiftL x 1) (shiftR y 1))

--this is an old unused function
reduce :: Int -> Int -> Int
reduce x r
    | x > r = reduce (xor x (extend x r)) r
    | otherwise = x

--this function combines remainder and multiplyaux to do modular multiplication in binary fields
multiplyint :: Int -> Int -> Int -> Int
multiplyint x y r = remainder (multiplyaux x y) r

--this function wraps the multiplyint function to let it take and return strings
multiply :: String -> String -> String
multiply xs ys = showHex (multiplyint (fromstring xs) (fromstring ys) 0x11b) ""

--Functions for binary field inversion

--engine for binary field division
divisionaux :: Int -> Int -> Int -> Int
divisionaux x y b
    | (bitlength x) < (bitlength y) = 0
    | b .&. x > 0 = xor b (divisionaux (xor x (extend x y)) y (shiftR b 1))
    | otherwise = divisionaux x y (shiftR b 1)

--provides correct inputs for divisionaux and then shortens result to correct length
division :: Int -> Int -> Int
division x y = shiftR (divisionaux x y (extend x 1)) ((bitlength y) - 1)

--finds remainder for binary field division
remainderaux :: Int -> Int -> Int -> Int
remainderaux x y b
    | (bitlength x) < (bitlength y) = x
    | b .&. x > 0 = (remainderaux (xor x (extend x y)) y (shiftR b 1))
    | otherwise = remainderaux x y (shiftR b 1)

--provides correct inputs to remainderaux
remainder :: Int -> Int -> Int
remainder x y = remainderaux x y (extend x 1)

--does binary field inversion
inversionaux :: Int -> Int -> Int -> Int -> Int
inversionaux k kk x r
    | remainder r x == 0 = k
    | otherwise = inversionaux (xor kk (multiplyaux k (division r x))) k (remainder r x) x

--provides correct inputs to inversionaux
inversion :: Int -> Int -> Int
inversion x r = inversionaux 1 0 x r

--wraps inversion to operate on strings and return a string
inverse :: String -> String
inverse s = showHex (inversion (fromstring s) 0x11b) ""
